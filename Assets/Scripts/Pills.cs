﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pills : MonoBehaviour {

    private int points;

    public bool isEnergizer;
    public bool isActivated;
    public bool isCherry;
    public bool isSpeed;
    public bool isLife;

    private void Start() {
        if (isEnergizer) {
            points = 50;
        } else {
            points = 10;
        }

        isActivated = false;
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag.Equals("Player") && GetComponent<Renderer>().enabled) {
            if (isEnergizer) {
                GameObject[] ghosts = GameObject.FindGameObjectsWithTag("Ghosts");

                foreach (GameObject ghost in ghosts) {
                    ghost.GetComponent<GhostMovement>().StartFrightenedMode();
                }
            }

            if (!GhostMovement.multiplayer) {
                if (isCherry) {
                    points = 100;
                    PillsControl.cherryItems = 0;
                    PillsControl.collected--; //Reaktivierung soll nicht den counter für die insgesamte Anzahl von Pillen beeinflussen
                                              //Deswegen hier dekrementiert, damit sich das Inkrementieren unten aufhebt

                    UIManager.cherryAvailable = false;
                }

                if (isSpeed) {
                    other.GetComponent<PlayerControl>().HasBoost();
                    PillsControl.speedItems = 0;
                    PillsControl.collected--;

                    UIManager.boostAvailable = false;
                }

                if (isLife) {
                    if (other.GetComponent<PlayerControl>().lives < 2) {
                        other.GetComponent<PlayerControl>().lives++;
                        PillsControl.lifeItems = 0;
                        PillsControl.collected--;

                        UIManager.lifeAvailable = false;
                    }
                }

                PillsControl.collected++;
                PillsControl.points += points;
            } else {
                if (isCherry) {
                    points = 100;
                    PillsControl.cherryItems = 0;
                    PillsControl.collected--;

                    UIManager.cherryAvailable = false;
                }
                if (isSpeed) {
                    other.GetComponent<MultiplayerControl>().HasBoost();
                    PillsControl.speedItems = 0;
                    PillsControl.collected--;

                    UIManager.boostAvailable = false;
                }

                if (isLife) {
                    if (other.GetComponent<MultiplayerControl>().lives < 2) {
                        other.GetComponent<MultiplayerControl>().lives++;
                        PillsControl.lifeItems = 0;
                        PillsControl.collected--;

                        UIManager.lifeAvailable = false;
                    }
                }

                PillsControl.collected++;
                other.GetComponent<MultiplayerControl>().points += points;
            }

            GetComponent<Renderer>().enabled = false;         

            isActivated = true;
        }
    }

}
