﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour {

    private float normalSpeed = 2.3f;
    private float boostSpeed = 5.3f;
    private float smoothRotation = 7.5f;
    bool col = false;

    private Vector3 currentDirection, nextDirection, previousDirection;
    private Waypoint currentWayPoint, previousWayPoint, targetWayPoint, spawnWayPoint;

    public int lives = 2;

    public Camera firstPersonCam;
    public Camera thirdPersonCam;

    public Canvas DeathScreen;
    public Canvas damage;

    public bool hasBoost;
    public float boostTime = 7.0f;
    private float boostTimer = 0.0f;
    public float speed;

    void Start() {

        currentDirection = Vector3.zero;
        spawnWayPoint = GameObject.Find("Waypoint (87)").GetComponent<Waypoint>();

        //Check ob am SpawnPoint, wenn nicht da hin teleportieren
        Waypoint wayPoint = GetWayPointAtCurrentPos();
        if (wayPoint != null && wayPoint == spawnWayPoint) {
            currentWayPoint = wayPoint;
        } else {
            transform.position = spawnWayPoint.transform.position;
            currentWayPoint = spawnWayPoint;
        }

        previousWayPoint = currentWayPoint;
        hasBoost = false;

        speed = normalSpeed;
    }

    void Update() {
        if (col == true)
        {
            damage.gameObject.SetActive(true);
            
        }


        CheckInput();
        Move();
        ChangeOrientation();
        CheckItems();

    }

    //Check User Input
    void CheckInput() {
        if (firstPersonCam.isActiveAndEnabled) {
            if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W)) {
                if (currentDirection == Vector3.zero) {
                    MoveInDirection(Vector3.left);
                }
            } else if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S)) {

                if (currentDirection == Vector3.zero && previousWayPoint != currentWayPoint) {
                    currentDirection = previousDirection;
                } else if (currentDirection == Vector3.zero && previousWayPoint == currentWayPoint) {
                    currentDirection = Vector3.left;
                }

                if (currentDirection == Vector3.left) {
                    MoveInDirection(Vector3.right);
                } else if (currentDirection == Vector3.right) {
                    MoveInDirection(Vector3.left);
                } else if (currentDirection == Vector3.forward) {
                    MoveInDirection(Vector3.back);
                } else if (currentDirection == Vector3.back) {
                    MoveInDirection(Vector3.forward);
                }
            } else if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D)) {
                if (currentDirection == Vector3.zero && previousWayPoint != currentWayPoint) {
                    currentDirection = previousDirection;
                } else if (currentDirection == Vector3.zero && previousWayPoint == currentWayPoint) {
                    currentDirection = Vector3.left;
                }

                if (currentDirection == Vector3.left) {
                    MoveInDirection(Vector3.forward);
                } else if (currentDirection == Vector3.forward) {
                    MoveInDirection(Vector3.right);
                } else if (currentDirection == Vector3.right) {
                    MoveInDirection(Vector3.back);
                } else if (currentDirection == Vector3.back) {
                    MoveInDirection(Vector3.left);
                }
            } else if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A)) {
                if (currentDirection == Vector3.zero && previousWayPoint != currentWayPoint) {
                    currentDirection = previousDirection;
                } else if (currentDirection == Vector3.zero && previousWayPoint == currentWayPoint) {
                    currentDirection = Vector3.left;
                }

                if (currentDirection == Vector3.left) {
                    MoveInDirection(Vector3.back);
                } else if (currentDirection == Vector3.back) {
                    MoveInDirection(Vector3.right);
                } else if (currentDirection == Vector3.right) {
                    MoveInDirection(Vector3.forward);
                } else if (currentDirection == Vector3.forward) {
                    MoveInDirection(Vector3.left);
                }
            } else if (Input.GetKeyDown(KeyCode.Space)) {
                ActivateBoost();
            }
        } else if (thirdPersonCam.isActiveAndEnabled) {
            if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W)) {
                MoveInDirection(Vector3.left);

            } else if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S)) {
                MoveInDirection(Vector3.right);

            } else if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D)) {
                MoveInDirection(Vector3.forward);


            } else if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A)) {
                MoveInDirection(Vector3.back);

            } else if (Input.GetKeyDown(KeyCode.Space)) {
                ActivateBoost();

            }
        }
    }

    //Stelle TargetWayPoint auf den WayPoint in der Direction
    void MoveInDirection(Vector3 direction) {
        if (direction != currentDirection) {
            nextDirection = direction;

            if (currentWayPoint != null) {
                Waypoint moveToWayPoint = WayPointInDirection(direction);

                if (moveToWayPoint != null) {
                    currentDirection = direction;
                    targetWayPoint = moveToWayPoint;
                    previousWayPoint = currentWayPoint;
                    currentWayPoint = null;
                }
            }
        }
    }

    //Bewegung zu einem WayPoint
    void Move() {
        if (targetWayPoint != currentWayPoint && targetWayPoint != null) {
            if (nextDirection == currentDirection * -1) {                                                   //Umdrehen
                currentDirection *= -1;

                Waypoint temp = targetWayPoint;
                targetWayPoint = previousWayPoint;
                previousWayPoint = temp;
            }

            if (OverShotTargetWayPoint()) {                                                                 //Nicht über den TargetWayPoint hinaus laufen 
                currentWayPoint = targetWayPoint;
                transform.position = currentWayPoint.transform.position;

                Waypoint receiver = GetPortal(currentWayPoint.transform.position);
                if (receiver != null) {
                    transform.position = receiver.transform.position;
                    currentWayPoint = receiver;
                }

                Waypoint moveToWayPoint = WayPointInDirection(nextDirection);
                if (moveToWayPoint != null) {
                    currentDirection = nextDirection;
                }

                if (moveToWayPoint == null) {
                    moveToWayPoint = WayPointInDirection(currentDirection);
                }

                if (moveToWayPoint != null) {
                    targetWayPoint = moveToWayPoint;
                    previousWayPoint = currentWayPoint;
                    currentWayPoint = null;
                } else {
                    previousDirection = currentDirection;
                    currentDirection = Vector3.zero;

                }
            } else {
                transform.position += currentDirection * speed * Time.deltaTime;                            //Bewegen solange noch nicht am TargetWayPoint vorbei
            }
        }
    }

    //Drehen des PacMans, damit er in die richtige Richtung guckt
    void ChangeOrientation() {
        if (firstPersonCam.isActiveAndEnabled) {
            if (currentDirection == Vector3.forward) {
                transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(0.0f, 0.0f, 0.0f), Time.deltaTime * smoothRotation);
            } else if (currentDirection == Vector3.back) {
                transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(0.0f, 180.0f, 0.0f), Time.deltaTime * smoothRotation);
            } else if (currentDirection == Vector3.right) {
                transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(0.0f, 90.0f, 0.0f), Time.deltaTime * smoothRotation);
            } else if (currentDirection == Vector3.left) {
                transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(0.0f, -90.0f, 0.0f), Time.deltaTime * smoothRotation);
            }
        } else if (thirdPersonCam.isActiveAndEnabled) {
            if (currentDirection == Vector3.forward) {
                transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
            } else if (currentDirection == Vector3.back) {
                transform.localRotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
            } else if (currentDirection == Vector3.right) {
                transform.localRotation = Quaternion.Euler(0.0f, 90.0f, 0.0f);
            } else if (currentDirection == Vector3.left) {
                transform.localRotation = Quaternion.Euler(0.0f, -90.0f, 0.0f);
            }
        }
    }

    //Gibt den WayPoint zurück dem man mit der Direction erreicht
    Waypoint WayPointInDirection(Vector3 direction) {
        Waypoint moveToWayPoint = null;

        for (int i = 0; i < currentWayPoint.neighbours.Length; i++) {
            if (currentWayPoint.validDirections[i] == direction) {
                moveToWayPoint = currentWayPoint.neighbours[i];
                break;
            }
        }

        return moveToWayPoint;
    }

    //Der WayPoint an der aktuellen Position
    Waypoint GetWayPointAtCurrentPos() {
        List<Waypoint> waypoints = GameObject.Find("Waypoints").GetComponent<WaypointsControl>().waypointsList;
        foreach (Waypoint waypoint in waypoints) {
            if (transform.position == waypoint.transform.position) {
                return waypoint;
            }
        }

        return null;
    }

    //Der Portal WayPoint
    Waypoint GetPortal(Vector3 pos) {
        List<Waypoint> waypoints = GameObject.Find("Waypoints").GetComponent<WaypointsControl>().waypointsList;
        foreach (Waypoint waypoint in waypoints) {
            if (pos == waypoint.transform.position) {
                if (waypoint.GetComponent<Waypoint>().isPortal) {
                    Waypoint receiver = waypoint.GetComponent<Waypoint>().receiver.GetComponent<Waypoint>();
                    return receiver;
                }
            }
        }

        return null;
    }

    //Über Target hinaus
    bool OverShotTargetWayPoint() {
        float distancePrevToTargetWayPoint = Vector3.Distance(targetWayPoint.transform.position, previousWayPoint.transform.position);
        float distancePrevToSelf = Vector3.Distance(transform.position, previousWayPoint.transform.position);

        return distancePrevToSelf > distancePrevToTargetWayPoint;       //Wenn die Distanz ziwschen PacMan und altem WayPoint > als die Distanz zwischen den WayPoints ist, ist man über dem target hinaus
    }

    //Collision mit Ghosts (nicht Frightened)
    public void CheckCollision() {
        col = true;
        lives--;

        if (lives == 0) {
            DeathScreen.gameObject.SetActive(true);
            Time.timeScale = 0.0f;
        }
    }

    private void CheckItems() {
        if (hasBoost) {
            boostTimer += Time.deltaTime;
            if (boostTimer > boostTime) {
                hasBoost = false;
                speed = normalSpeed;
                boostTimer = 0.0f;
            }
        }
    }

    public void HasBoost() {
        if (!hasBoost) {
            hasBoost = true;
            boostTimer = 0.0f;
        }
    }

    public void ActivateBoost() {
        if (hasBoost) {
            speed = boostSpeed;
        }
    }

    public Waypoint GetWaypoint() {
        return currentWayPoint;
    }

    public Waypoint GetTargetWayPoint() {
        return targetWayPoint;
    }

    public Vector3 GetDirection() {
        return currentDirection;
    }
}