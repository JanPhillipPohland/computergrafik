﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplayerCamera : MonoBehaviour {
    public Camera TPP;
    public static bool FPP = true;

    private void Update()
    {
        if (FPP)
        {
            TPP.enabled = false;
        }
        else
        {
            TPP.enabled = true;
        }
    }

}
