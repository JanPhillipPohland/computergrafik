﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTracking : MonoBehaviour {

    public GameObject target;
    private Vector3 offsetP;
    
    // Use this for initialization
    void Start() {
        offsetP = transform.position - target.transform.position;
        

    }

    // Update is called once per frame
    void Update() {
        transform.position = target.transform.position + target.transform.rotation * offsetP;
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, target.transform.rotation.eulerAngles.y, 0);

    }
}
