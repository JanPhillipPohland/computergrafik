﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

    bool paused = false;

    Text points3rdPerson;
    Text timer;

    public Canvas pause;
    GameObject lives;
    GameObject items;

    GameObject player;

    public static CamMode camMode = CamMode.fp;
    private string pointsPrefix = "Points: ";
    private string timerPrefix = "Time: ";
    private float time = 0.0f;

    Text cherryText;
    Text boostText;
    Text lifeText;

    public static bool cherryAvailable;
    public static bool boostAvailable;
    public static bool lifeAvailable;

    public enum CamMode {
        fp, tp
    }

    public GameObject LiveSprite1;
    public GameObject LiveSprite2;

    // Use this for initialization
    void Start() {

        if (!GhostMovement.multiplayer) {
            points3rdPerson = GameObject.Find("Points3rdPerson").GetComponent<Text>();
            lives = GameObject.Find("LifeSprites");
            player = GameObject.Find("PacMan");
            items = GameObject.Find("Items");
            timer = GameObject.Find("Timer").GetComponent<Text>();

            cherryText = transform.Find("TPP").transform.Find("Items").transform.Find("CherryText").GetComponent<Text>();
            boostText = transform.Find("TPP").transform.Find("Items").transform.Find("BoostText").GetComponent<Text>();
            lifeText = transform.Find("TPP").transform.Find("Items").transform.Find("LifeText").GetComponent<Text>();

            cherryAvailable = false;
            boostAvailable = false;
            lifeAvailable = false;
        }
    }

    // Update is called once per frame
    void Update() {


        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (paused) {
                Time.timeScale = 1.0f;
                pause.gameObject.SetActive(false);
                paused = false;
            } else {
                Time.timeScale = 0.0f;
                pause.gameObject.SetActive(true);
                paused = true;
            }
        }

        time += Time.deltaTime;

        if (!GhostMovement.multiplayer) {
            if (player.GetComponent<PlayerControl>().lives == 1) {
                lives.transform.GetChild(2).GetComponent<Image>().enabled = false;
            } else if (player.GetComponent<PlayerControl>().lives == 0) {
                lives.transform.GetChild(1).GetComponent<Image>().enabled = false;
            }
        }

        if (camMode == CamMode.fp) {
            if (!GhostMovement.multiplayer) {
                points3rdPerson.enabled = false;
                lives.SetActive(false);
                items.SetActive(false);
                timer.enabled = false;
            }
        } else if (camMode == CamMode.tp) {

            if (!GhostMovement.multiplayer) {
                points3rdPerson.enabled = true;
                points3rdPerson.text = pointsPrefix + PillsControl.points;
                points3rdPerson.color = new Color(1.0f, 1.0f, 1.0f, Mathf.PingPong(Time.time * 3.5f, 1.0f));
                lives.SetActive(true);
                items.SetActive(true);

            }

            if (!GhostMovement.multiplayer) {
                timer.enabled = true;
                timer.text = timerPrefix + (int)time;
                timer.color = new Color(1.0f, 1.0f, 1.0f, Mathf.PingPong(Time.time * 3.5f, 1.0f));
            } else {
                timer.enabled = false;
            }
        }

        if (!GhostMovement.multiplayer) {
            if (cherryAvailable) {
                cherryText.enabled = true;
                cherryText.color = new Color(1.0f, 1.0f, 1.0f, Mathf.PingPong(Time.time * 1.5f, 1.0f));
            } else {
                cherryText.enabled = false;
            }

            if (boostAvailable) {
                boostText.enabled = true;
                boostText.color = new Color(1.0f, 1.0f, 1.0f, Mathf.PingPong(Time.time * 1.5f, 1.0f));
            } else {
                boostText.enabled = false;
            }

            if (lifeAvailable) {
                lifeText.enabled = true;
                lifeText.color = new Color(1.0f, 1.0f, 1.0f, Mathf.PingPong(Time.time * 1.5f, 1.0f));
            } else {
                lifeText.enabled = false;
            }
        }
    }

    public void closePause() {
        Time.timeScale = 1.0f;
        pause.gameObject.SetActive(false);
        paused = false;
    }
    public void mainMenu() {
        MultiplayerCamera.FPP = true;
        CameraSwitch.firstPAllowed = true;
        CameraSwitch.thirdPAllowed = true;
        SceneManager.LoadScene("MainMenu");
        Time.timeScale = 1.0f;
    }
}
