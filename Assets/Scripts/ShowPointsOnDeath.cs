﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowPointsOnDeath : MonoBehaviour {

    public Text pointsPlayer1;
    public Text pointsPlayer2;

    GameObject player1;
    GameObject player2;
    void Start() {
        player1 = GameObject.Find("MultiplayerPacMan");
        player2 = GameObject.Find("MultiplayerPacMan2");
    }

    void Update() {
        SetPoints();
    }

    void SetPoints() {
        pointsPlayer1.text = "Points Player1: " + player1.GetComponent<MultiplayerControl>().points;
        pointsPlayer2.text = "Points Player2: " + player2.GetComponent<MultiplayerControl>().points;
    }
}
