﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minimap : MonoBehaviour {

    public GameObject player;
    public GameObject lights;

    // Update is called once per frame
    void LateUpdate() {
        Vector3 newPos = player.transform.position;
        newPos.y = transform.position.y;

        transform.position = newPos;
    }
}
