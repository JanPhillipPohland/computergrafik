﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**
 * Siehe PlayerControl größtenteils
 */
public class MultiplayerControl : MonoBehaviour {

    private float normalSpeed = 2.3f;
    private float boostSpeed = 5.3f;
    private float smoothRotation = 7.5f;
    bool col = false;

    private Vector3 currentDirection, nextDirection, previousDirection;
    private Waypoint currentWayPoint, previousWayPoint, targetWayPoint, spawnWayPoint;

    private Waypoint[] spawnWayPoints;

    public int lives = 2;

    public Camera firstPersonCam;
    public Camera thirdPersonCam;

    public Canvas DeathScreen;
    public Canvas damage;

    public bool hasBoost;
    public float boostTime = 7.0f;
    private float boostTimer = 0.0f;
    public float speed;

    public int playerNumber;
    public int points;
    void Start() {

        currentDirection = Vector3.zero;

        if (name.Equals("MultiplayerPacMan")) {
            playerNumber = 1;
            spawnWayPoint = GameObject.Find("Waypoint (51)").GetComponent<Waypoint>();
        } else if (name.Equals("MultiplayerPacMan2")) {
            playerNumber = 2;
            spawnWayPoint = GameObject.Find("Waypoint (26)").GetComponent<Waypoint>();
        }

        Waypoint wayPoint = GetWayPointAtPos(transform.position);
        if (wayPoint != null && wayPoint == spawnWayPoint) {
            currentWayPoint = wayPoint;
        } else {
            transform.position = spawnWayPoint.transform.position;
            currentWayPoint = spawnWayPoint;
        }

        previousWayPoint = currentWayPoint;
        hasBoost = false;

        speed = normalSpeed;
    }

    void Update() {
        if (col == true && !damage.isActiveAndEnabled && !MultiplayerCamera.FPP) {
            damage.gameObject.SetActive(true);

        }

        CheckInput();
        Move();
        ChangeOrientation();
        CheckItems();

    }

    void CheckInput() {
        if (!MultiplayerCamera.FPP)
        {
            if (playerNumber == 1)
            {
                if (Input.GetKeyDown(KeyCode.W))
                {
                    ChangePosition(Vector3.left);

                }
                else if (Input.GetKeyDown(KeyCode.S))
                {
                    ChangePosition(Vector3.right);

                }
                else if (Input.GetKeyDown(KeyCode.D))
                {
                    ChangePosition(Vector3.forward);


                }
                else if (Input.GetKeyDown(KeyCode.A))
                {
                    ChangePosition(Vector3.back);

                }
                else if (Input.GetKeyDown(KeyCode.Space))
                {
                    ActivateBoost();

                }
            }
            else if (playerNumber == 2)
            {
                if (Input.GetKeyDown(KeyCode.UpArrow))
                {
                    ChangePosition(Vector3.left);

                }
                else if (Input.GetKeyDown(KeyCode.DownArrow))
                {
                    ChangePosition(Vector3.right);

                }
                else if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    ChangePosition(Vector3.forward);


                }
                else if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    ChangePosition(Vector3.back);

                }
                else if (Input.GetKeyDown(KeyCode.Return))
                {
                    ActivateBoost();

                }
            }
        }
        else
        {
            if(playerNumber == 1)
            {
                if (Input.GetKeyDown(KeyCode.W))
                {
                    if (currentDirection == Vector3.zero)
                    {
                        ChangePosition(Vector3.left);
                    }
                }
                else if (Input.GetKeyDown(KeyCode.S))
                {

                    if (currentDirection == Vector3.zero && previousWayPoint != currentWayPoint)
                    {
                        currentDirection = previousDirection;
                    }
                    else if (currentDirection == Vector3.zero && previousWayPoint == currentWayPoint)
                    {
                        currentDirection = Vector3.left;
                    }

                    if (currentDirection == Vector3.left)
                    {
                        ChangePosition(Vector3.right);
                    }
                    else if (currentDirection == Vector3.right)
                    {
                        ChangePosition(Vector3.left);
                    }
                    else if (currentDirection == Vector3.forward)
                    {
                        ChangePosition(Vector3.back);
                    }
                    else if (currentDirection == Vector3.back)
                    {
                        ChangePosition(Vector3.forward);
                    }
                }
                else if (Input.GetKeyDown(KeyCode.D))
                {
                    if (currentDirection == Vector3.zero && previousWayPoint != currentWayPoint)
                    {
                        currentDirection = previousDirection;
                    }
                    else if (currentDirection == Vector3.zero && previousWayPoint == currentWayPoint)
                    {
                        currentDirection = Vector3.left;
                    }

                    if (currentDirection == Vector3.left)
                    {
                        ChangePosition(Vector3.forward);
                    }
                    else if (currentDirection == Vector3.forward)
                    {
                        ChangePosition(Vector3.right);
                    }
                    else if (currentDirection == Vector3.right)
                    {
                        ChangePosition(Vector3.back);
                    }
                    else if (currentDirection == Vector3.back)
                    {
                        ChangePosition(Vector3.left);
                    }
                }
                else if (Input.GetKeyDown(KeyCode.A))
                {
                    if (currentDirection == Vector3.zero && previousWayPoint != currentWayPoint)
                    {
                        currentDirection = previousDirection;
                    }
                    else if (currentDirection == Vector3.zero && previousWayPoint == currentWayPoint)
                    {
                        currentDirection = Vector3.left;
                    }

                    if (currentDirection == Vector3.left)
                    {
                        ChangePosition(Vector3.back);
                    }
                    else if (currentDirection == Vector3.back)
                    {
                        ChangePosition(Vector3.right);
                    }
                    else if (currentDirection == Vector3.right)
                    {
                        ChangePosition(Vector3.forward);
                    }
                    else if (currentDirection == Vector3.forward)
                    {
                        ChangePosition(Vector3.left);
                    }
                }
                else if (Input.GetKeyDown(KeyCode.Space))
                {
                    ActivateBoost();

                }
            }
            else if(playerNumber == 2)
            {
                if (Input.GetKeyDown(KeyCode.UpArrow))
                {
                    if (currentDirection == Vector3.zero)
                    {
                        ChangePosition(Vector3.left);
                    }
                }
                else if (Input.GetKeyDown(KeyCode.DownArrow))
                {

                    if (currentDirection == Vector3.zero && previousWayPoint != currentWayPoint)
                    {
                        currentDirection = previousDirection;
                    }
                    else if (currentDirection == Vector3.zero && previousWayPoint == currentWayPoint)
                    {
                        currentDirection = Vector3.left;
                    }

                    if (currentDirection == Vector3.left)
                    {
                        ChangePosition(Vector3.right);
                    }
                    else if (currentDirection == Vector3.right)
                    {
                        ChangePosition(Vector3.left);
                    }
                    else if (currentDirection == Vector3.forward)
                    {
                        ChangePosition(Vector3.back);
                    }
                    else if (currentDirection == Vector3.back)
                    {
                        ChangePosition(Vector3.forward);
                    }
                }
                else if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    if (currentDirection == Vector3.zero && previousWayPoint != currentWayPoint)
                    {
                        currentDirection = previousDirection;
                    }
                    else if (currentDirection == Vector3.zero && previousWayPoint == currentWayPoint)
                    {
                        currentDirection = Vector3.left;
                    }

                    if (currentDirection == Vector3.left)
                    {
                        ChangePosition(Vector3.forward);
                    }
                    else if (currentDirection == Vector3.forward)
                    {
                        ChangePosition(Vector3.right);
                    }
                    else if (currentDirection == Vector3.right)
                    {
                        ChangePosition(Vector3.back);
                    }
                    else if (currentDirection == Vector3.back)
                    {
                        ChangePosition(Vector3.left);
                    }
                }
                else if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    if (currentDirection == Vector3.zero && previousWayPoint != currentWayPoint)
                    {
                        currentDirection = previousDirection;
                    }
                    else if (currentDirection == Vector3.zero && previousWayPoint == currentWayPoint)
                    {
                        currentDirection = Vector3.left;
                    }

                    if (currentDirection == Vector3.left)
                    {
                        ChangePosition(Vector3.back);
                    }
                    else if (currentDirection == Vector3.back)
                    {
                        ChangePosition(Vector3.right);
                    }
                    else if (currentDirection == Vector3.right)
                    {
                        ChangePosition(Vector3.forward);
                    }
                    else if (currentDirection == Vector3.forward)
                    {
                        ChangePosition(Vector3.left);
                    }
                }
                else if (Input.GetKeyDown(KeyCode.Return))
                {
                    ActivateBoost();

                }
            }
        }
        
    }

    void ChangePosition(Vector3 direction) {
        if (direction != currentDirection) {
            nextDirection = direction;

            if (currentWayPoint != null) {
                Waypoint moveToWayPoint = CanMoveTo(direction);

                if (moveToWayPoint != null) {
                    currentDirection = direction;
                    targetWayPoint = moveToWayPoint;
                    previousWayPoint = currentWayPoint;
                    currentWayPoint = null;
                }
            }
        }
    }

    void Move() {
        if (targetWayPoint != currentWayPoint && targetWayPoint != null) {
            if (nextDirection == currentDirection * -1) {
                currentDirection *= -1;

                Waypoint temp = targetWayPoint;
                targetWayPoint = previousWayPoint;
                previousWayPoint = temp;
            }

            if (OverShotTargetWayPoint()) {
                currentWayPoint = targetWayPoint;
                transform.position = currentWayPoint.transform.position;

                Waypoint receiver = GetPortal(currentWayPoint.transform.position);
                if (receiver != null) {
                    transform.position = receiver.transform.position;
                    currentWayPoint = receiver;
                }

                Waypoint moveToWayPoint = CanMoveTo(nextDirection);
                if (moveToWayPoint != null) {
                    currentDirection = nextDirection;
                }

                if (moveToWayPoint == null) {
                    moveToWayPoint = CanMoveTo(currentDirection);
                }

                if (moveToWayPoint != null) {
                    targetWayPoint = moveToWayPoint;
                    previousWayPoint = currentWayPoint;
                    currentWayPoint = null;
                } else {
                    previousDirection = currentDirection;
                    currentDirection = Vector3.zero;

                }
            } else {
                transform.position += currentDirection * speed * Time.deltaTime;
            }
        }
    }
    
    void ChangeOrientation() {
        if (!MultiplayerCamera.FPP) {
            if (currentDirection == Vector3.forward) {
                transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
            } else if (currentDirection == Vector3.back) {
                transform.localRotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
            } else if (currentDirection == Vector3.right) {
                transform.localRotation = Quaternion.Euler(0.0f, 90.0f, 0.0f);
            } else if (currentDirection == Vector3.left) {
                transform.localRotation = Quaternion.Euler(0.0f, -90.0f, 0.0f);
            }
        }
        if (MultiplayerCamera.FPP)
        {
            if (currentDirection == Vector3.forward)
            {
                transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(0.0f, 0.0f, 0.0f), Time.deltaTime * smoothRotation);
            }
            else if (currentDirection == Vector3.back)
            {
                transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(0.0f, 180.0f, 0.0f), Time.deltaTime * smoothRotation);
            }
            else if (currentDirection == Vector3.right)
            {
                transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(0.0f, 90.0f, 0.0f), Time.deltaTime * smoothRotation);
            }
            else if (currentDirection == Vector3.left)
            {
                transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(0.0f, -90.0f, 0.0f), Time.deltaTime * smoothRotation);
            }
        }
    }

    Waypoint CanMoveTo(Vector3 direction) {
        Waypoint moveToWayPoint = null;

        for (int i = 0; i < currentWayPoint.neighbours.Length; i++) {
            if (currentWayPoint.validDirections[i] == direction) {
                moveToWayPoint = currentWayPoint.neighbours[i];
                break;
            }
        }

        return moveToWayPoint;
    }

    Waypoint GetWayPointAtPos(Vector3 pos) {
        List<Waypoint> waypoints = GameObject.Find("Waypoints").GetComponent<WaypointsControl>().waypointsList;
        foreach (Waypoint waypoint in waypoints) {
            if (pos == waypoint.transform.position) {
                return waypoint;
            }
        }

        return null;
    }

    Waypoint GetPortal(Vector3 pos) {
        List<Waypoint> waypoints = GameObject.Find("Waypoints").GetComponent<WaypointsControl>().waypointsList;
        foreach (Waypoint waypoint in waypoints) {
            if (pos == waypoint.transform.position) {
                if (waypoint.GetComponent<Waypoint>().isPortal) {
                    Waypoint receiver = waypoint.GetComponent<Waypoint>().receiver.GetComponent<Waypoint>();
                    return receiver;
                }
            }
        }

        return null;
    }

    bool OverShotTargetWayPoint() {
        float distancePrevToTargetWayPoint = Vector3.Distance(targetWayPoint.transform.position, previousWayPoint.transform.position);
        float distancePrevToSelf = Vector3.Distance(transform.position, previousWayPoint.transform.position);

        return distancePrevToSelf > distancePrevToTargetWayPoint;
    }

    public void CheckCollision() {
        col = true;
        lives--;

        if (lives == 0) {
            DeathScreen.gameObject.SetActive(true);
            Time.timeScale = 0.0f;
        }
    }

    private void CheckItems() {
        if (hasBoost) {
            boostTimer += Time.deltaTime;
            if (boostTimer > boostTime) {
                hasBoost = false;
                speed = normalSpeed;
                boostTimer = 0.0f;
            }
        }
    }

    public void HasBoost() {
        if (!hasBoost) {
            hasBoost = true;
            boostTimer = 0.0f;
        }
    }

    public void ActivateBoost() {
        if (hasBoost) {
            speed = boostSpeed;
        }
    }

    public Waypoint GetWaypoint() {
        return currentWayPoint;
    }

    public Waypoint GetTargetWayPoint() {
        return targetWayPoint;
    }

    public Vector3 GetDirection() {
        return currentDirection;
    }

    //Collision von hinten mit anderem Player
    public void CheckMultiPlayerCollision() {
        lives--;

        if (lives == 0) {
            DeathScreen.gameObject.SetActive(true);
            Time.timeScale = 0.0f;
        }
    }

    //Collision wenn beide sich angucken lässt sie zurück bouncen 
    //und gibt einen sehr kurzen boost damit sie weit genug voneinander weg kommen 
    //und nicht direkt wieder gegeneinander kommen
    public void BounceBack() {
        speed = 10.3f;
        ChangePosition(currentDirection * -1);

        StartCoroutine(ResetSpeedAfterBounce(0.5f));
    }

    IEnumerator ResetSpeedAfterBounce(float time) {
        yield return new WaitForSeconds(time);
        speed = normalSpeed;
    }
}
