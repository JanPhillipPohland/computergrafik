﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiPlayerTrigger : MonoBehaviour {

    //Collision zwischen den beiden Playern wird hier abgefangen, von Seite / Hinten oder Vorne, d.h. beide gucken sich an
    private void OnTriggerEnter(Collider other) {
        if (other.tag.Equals("Player") && other.GetComponent<MultiplayerControl>().GetDirection() != GetComponentInParent<MultiplayerControl>().GetDirection() * -1) {
            GetComponentInParent<MultiplayerControl>().CheckMultiPlayerCollision();
        } else if (other.tag.Equals("Player") && other.GetComponent<MultiplayerControl>().GetDirection() == GetComponentInParent<MultiplayerControl>().GetDirection() * -1) {
            other.GetComponent<MultiplayerControl>().BounceBack();
            GetComponentInParent<MultiplayerControl>().BounceBack();
        }
    }
}
