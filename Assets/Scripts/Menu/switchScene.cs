﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class switchScene : MonoBehaviour {
    public GameObject Camera;
    public Canvas SP;
    public Canvas MP;
    public Canvas C;


    private bool rotateUntermenu = false;
    private bool rotateMainmenu = false;
    private Vector3 targetAngle = new Vector3(0.0f, 290.0f, 0.0f);

    public void LoadSingleplayer() {
        SP.gameObject.SetActive(true);
        rotateUntermenu = true;
        Debug.Log("camera gedreht");
    }
    public void LoadMultiplayer() {
        MP.gameObject.SetActive(true);
        rotateUntermenu = true;
        
    }
    
    public void loadControls() {
        C.gameObject.SetActive(true);
        rotateUntermenu = true;
    }
    public void QuitGame() {
        Application.Quit();
    }
    public void gotoSingleplayer() {
        if(CameraSwitch.thirdPAllowed || CameraSwitch.firstPAllowed)
        {
            GhostMovement.multiplayer = false;
            SceneManager.LoadScene("Scene");
        }
    }

    public void gotoMultiplayer()
    {
        if (CameraSwitch.thirdPAllowed || CameraSwitch.firstPAllowed)
        {
            GhostMovement.multiplayer = true;
            SceneManager.LoadScene("Scene 1");
        }
    }
    public void BackToMainMenu() {
        rotateMainmenu = true;
    }
    public bool SPPressed() {
        return true;
    }

    public void switchMPFPP()
    {
        if (MultiplayerCamera.FPP)
        {
            MultiplayerCamera.FPP = false;
        }
        else
        {
            MultiplayerCamera.FPP = true;
        }
        
    }
    

    public void switchThirdPAllowed()
    {
        if (CameraSwitch.thirdPAllowed)
        {
            CameraSwitch.thirdPAllowed = false;
            Debug.Log("3rd P: " + CameraSwitch.thirdPAllowed);
        }
        else
        {
            CameraSwitch.thirdPAllowed = true;
            Debug.Log("3rd P: " + CameraSwitch.thirdPAllowed);

        }

    }
    public void switchFirstAllowed()
    {
        if (CameraSwitch.firstPAllowed)
        {
            CameraSwitch.firstPAllowed = false;
        }
        else
        {
            CameraSwitch.firstPAllowed = true;
        }
        Debug.Log("1st P: " + CameraSwitch.firstPAllowed);

    }


    private void Update() {
        if (rotateUntermenu) {
            if (targetAngle.y < Camera.transform.eulerAngles.y) {
                Camera.transform.Rotate(new Vector3(0.0f, -4.0f, 0.0f));
            } else {

                rotateUntermenu = false;
            }
        } else if (rotateMainmenu) {
            if (Camera.transform.eulerAngles.y < 350.0f) {
                Camera.transform.Rotate(new Vector3(0.0f, 4.0f, 0.0f));
            } else {
                SP.gameObject.SetActive(false);
                MP.gameObject.SetActive(false);
                C.gameObject.SetActive(false);
                rotateMainmenu = false;
            }
        }

    }
}
