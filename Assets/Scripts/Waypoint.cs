﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour {

    public Waypoint[] neighbours;
    public Vector3[] validDirections;

    public bool isPortal;
    public GameObject receiver;

    // Use this for initialization
    void Start() {
        validDirections = new Vector3[neighbours.Length];

        for (int i = 0; i < neighbours.Length; i++) {
            Waypoint neighbor = neighbours[i];
            Vector3 temp = neighbor.transform.localPosition - transform.localPosition;                              //rechnet Offset zwischen benachbarten WayPoints aus, der dann normalisiert die Richtung angibt mit der man zu diesem WayPoint vom aktuellen gelangt

            validDirections[i] = temp.normalized;
        }
    }

    //Visuelle Darstellung der WayPoints in Scene View zur Übersicht
    private void OnDrawGizmos() {
        float radius = 0.2f;
        Gizmos.color = Color.white;

        if (name.Equals("Waypoint Scatter Blinky")) {
            Gizmos.color = new Color(0.815f, 0.243f, 0.098f);
            radius = 0.5f;
        } else if (name.Equals("Waypoint Scatter Pinky")) {
            Gizmos.color = new Color(0.917f, 0.509f, 0.898f);
            radius = 0.5f;
        } else if (name.Equals("Waypoint Scatter Inky")) {
            Gizmos.color = new Color(0.274f, 0.749f, 0.933f);
            radius = 0.5f;
        } else if (name.Equals("Waypoint Scatter Clyde")) {
            Gizmos.color = new Color(0.858f, 0.521f, 0.109f);
            radius = 0.5f;
        }

        Gizmos.DrawSphere(transform.position, radius);
    }
}