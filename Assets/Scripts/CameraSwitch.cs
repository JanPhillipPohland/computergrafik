﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitch : MonoBehaviour {

    public Camera firstPCamera;
    public Camera thirdPCamera;

    public static bool firstPAllowed = true;
    public static bool thirdPAllowed = true;

    GameObject minimap;
    GameObject portal;
    // Use this for initialization
    void Start() {
        if (!firstPAllowed)
        {
            firstPCamera.enabled = false;
            thirdPCamera.enabled = true;
        }
        firstPCamera.enabled = true;
        thirdPCamera.enabled = false;

        thirdPCamera.GetComponent<AudioListener>().enabled = false;

        minimap = GameObject.Find("Minimap");
               minimap.SetActive(true);

        portal = GameObject.Find("Portale");
        portal.SetActive(true);
    }

    // Update is called once per frame
    void Update() {
        if(firstPAllowed && thirdPAllowed)
        {
            if (Input.GetKeyDown(KeyCode.LeftControl) && !firstPCamera.isActiveAndEnabled)
            {
                firstPCamera.enabled = true;
                thirdPCamera.enabled = false;

                firstPCamera.GetComponent<AudioListener>().enabled = true;
                thirdPCamera.GetComponent<AudioListener>().enabled = false;

                minimap.SetActive(true);
                portal.SetActive(true);

                UIManager.camMode = UIManager.CamMode.fp;
            }
            else if (Input.GetKeyDown(KeyCode.LeftControl) && !thirdPCamera.isActiveAndEnabled)
            {
                firstPCamera.enabled = false;
                thirdPCamera.enabled = true;

                thirdPCamera.GetComponent<AudioListener>().enabled = true;
                firstPCamera.GetComponent<AudioListener>().enabled = false;

                minimap.SetActive(false);
                portal.SetActive(false);

                UIManager.camMode = UIManager.CamMode.tp;
            }
        }else if (firstPAllowed)
        {
            firstPCamera.enabled = true;
            thirdPCamera.enabled = false;

            firstPCamera.GetComponent<AudioListener>().enabled = true;
            thirdPCamera.GetComponent<AudioListener>().enabled = false;

            minimap.SetActive(true);
            portal.SetActive(true);

            UIManager.camMode = UIManager.CamMode.fp;
        }else if (thirdPAllowed)
        {
            firstPCamera.enabled = false;
            thirdPCamera.enabled = true;

            thirdPCamera.GetComponent<AudioListener>().enabled = true;
            firstPCamera.GetComponent<AudioListener>().enabled = false;

            minimap.SetActive(false);
            portal.SetActive(false);

            UIManager.camMode = UIManager.CamMode.tp;
        }

        
    }
}
