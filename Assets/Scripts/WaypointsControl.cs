﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointsControl : MonoBehaviour {

    public List<Waypoint> waypointsList = new List<Waypoint>();

    // Use this for initialization
    void Start() {
        GameObject[] waypoints = GameObject.FindGameObjectsWithTag("Waypoints");

        foreach (GameObject obj in waypoints) {
            if (!obj.name.Equals("Waypoints")) {
                waypointsList.Add(obj.GetComponent<Waypoint>());
            }
        }
    }
}
