﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PillsControl : MonoBehaviour {

    public List<Pills> pillsList = new List<Pills>();

    public Canvas winningScreen;

    public static int collected = 0;
    public static int points = 0;
    int maxCherryItems = 1;
    public static int cherryItems = 0;

    int maxSpeedItems = 1;
    public static int speedItems = 0;

    int maxLifeItems = 1;
    public static int lifeItems = 0;
    // Use this for initialization
    void Start() {
        GameObject[] pills = GameObject.FindGameObjectsWithTag("Pills");

        foreach (GameObject obj in pills) {
            if (!obj.name.Equals("Pills")) {
                pillsList.Add(obj.GetComponent<Pills>());
            }
        }
    }

    // Update is called once per frame
    void Update() {
        if (collected == pillsList.Count) {
            winningScreen.gameObject.SetActive(true);
            Debug.Log("You Won!");
        }

        if (collected == 100 || collected == 200 || collected == 300) {                                     //Jede 100. Pille wird eine Cherry gespawnt (mehr Punkte)
            if (cherryItems < maxCherryItems) {
                Pills[] pillObj = pillsList.ToArray();
                Pills[] collectedPills = new Pills[collected];
                int collectedPillsCounter = 0;
                for (int i = 0; i < pillObj.Length; i++) {
                    if (pillObj[i].isActivated && collectedPillsCounter < collectedPills.Length && !pillObj[i].isEnergizer) {

                        collectedPills[collectedPillsCounter] = pillObj[i];
                        collectedPillsCounter++;
                    }
                }

                int index = Random.Range(0, collectedPills.Length);

                collectedPills[index].GetComponent<Renderer>().enabled = true;
                collectedPills[index].GetComponent<Renderer>().material.color = Color.red;
                collectedPills[index].isCherry = true;
                collectedPills[index].isActivated = false;

                cherryItems = 1;

                UIManager.cherryAvailable = true;
            }
        }
            
        if (collected % 30 == 0 && collected != 300 && collected != 0) {                                    //Jede 30. SpeedBoost
            if (speedItems < maxSpeedItems) {
                Pills[] pillObj = pillsList.ToArray();
                Pills[] collectedPills = new Pills[collected];

                int collectedPillsCounter = 0;
                for (int i = 0; i < pillObj.Length; i++) {
                    if (pillObj[i].isActivated && collectedPillsCounter < collectedPills.Length && !pillObj[i].isEnergizer) {

                        collectedPills[collectedPillsCounter] = pillObj[i];
                        collectedPillsCounter++;
                    }
                }

                int index = Random.Range(0, collectedPills.Length);

                collectedPills[index].GetComponent<Renderer>().enabled = true;
                collectedPills[index].GetComponent<Renderer>().material.color = new Color(135.0f / 255.0f, 206.0f / 255.0f, 250.0f / 255.0f);
                collectedPills[index].isSpeed = true;
                collectedPills[index].isActivated = false;

                speedItems = 1;

                UIManager.boostAvailable = true;
            }
        }

        if (collected % 80 == 0 && collected != 0) {                                                        //Jede 80. Extra Life
            if (lifeItems < maxLifeItems) {
                Pills[] pillObj = pillsList.ToArray();
                Pills[] collectedPills = new Pills[collected];

                int collectedPillsCounter = 0;
                for (int i = 0; i < pillObj.Length; i++) {
                    if (pillObj[i].isActivated && collectedPillsCounter < collectedPills.Length && !pillObj[i].isEnergizer) {
                        collectedPills[collectedPillsCounter] = pillObj[i];
                        collectedPillsCounter++;
                    }
                }

                int index = Random.Range(0, collectedPills.Length);

                collectedPills[index].GetComponent<Renderer>().enabled = true;
                collectedPills[index].GetComponent<Renderer>().material.color = new Color(255.0f / 255.0f, 140.0f / 255.0f, 0.0f);
                collectedPills[index].isLife = true;
                collectedPills[index].isActivated = false;

                lifeItems = 1;

                UIManager.lifeAvailable = true;
            }
        }
    }
}