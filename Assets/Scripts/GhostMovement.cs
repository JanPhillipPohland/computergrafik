﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class GhostMovement : MonoBehaviour {

    private float normalSpeed = 2.3f;
    private float frightenedSpeed = 1.3f;
    //private float consumedSpeed = 20.3f;

    private float scatterTime1 = 7.0f;
    private float scatterTime2 = 5.0f;
    private float chaseTime = 20.0f;
    private float frightenedTime = 10.0f;
    private float blinkingTime = 7.0f;          //TODO Model ändern
    private float releaseTimeGhost;
    private float releaseTimer = 0;

    private float changeModeTimer = 0.0f;
    private float changeModeCount = 1.0f;
    private float frightenedModeTimer = 0.0f;
    private float blinkTimer = 0.0f;

    private Vector3 currentDirection, nextDirection;
    private Waypoint currentWayPoint, previousWayPoint, targetWayPoint, scatterWayPoint, spawnWayPoint;

    private Transform sprite;
    private Quaternion spriteRotation;

    private Mesh initialMesh;
    private Material initialMaterial;

    public Mesh frightenedMesh;
    public Material frightenedMaterial;

    public Mesh blinkMesh;
    public Material blinkMaterial;

    public GameObject[] players;
    public GameObject player;
    public float speed;

    public GhostType ghostType;

    public bool isReleased = false;
    public bool isInHouse = true;

    public enum GhostType {
        Blinky, Pinky, Inky, Clyde
    }

    private enum Mode {
        Chase, Scatter, Frightened, Consumed
    }

    Mode currentMode = Mode.Scatter;
    Mode previousMode;

    public static bool multiplayer = false;

    // Use this for initialization
    void Start() {
        if (ghostType == GhostType.Blinky) {
            releaseTimeGhost = 0;
        } else if (ghostType == GhostType.Pinky) {
            releaseTimeGhost = 5;
        } else if (ghostType == GhostType.Inky) {
            releaseTimeGhost = 10;
        } else if (ghostType == GhostType.Clyde) {
            releaseTimeGhost = 15;
        }

        speed = normalSpeed;

        scatterWayPoint = GetScatterWayPoint();

        currentWayPoint = GetWayPointAtPos(transform.position);

        spawnWayPoint = currentWayPoint;

        sprite = transform.GetChild(0);
        spriteRotation = sprite.rotation;

        StartCoroutine(WaitToRelease(releaseTimeGhost));

        previousWayPoint = currentWayPoint;

        initialMesh = GetComponent<MeshFilter>().mesh;
        initialMaterial = GetComponent<Renderer>().material;

        transform.GetChild(1).gameObject.GetComponent<Renderer>().enabled = false;
        transform.GetChild(2).gameObject.GetComponent<Renderer>().enabled = false;

        players = GameObject.FindGameObjectsWithTag("Player");
    }

    // Update is called once per frame
    void Update() {
        if (multiplayer && Time.frameCount % 20 == 0) {
            UpdatePlayer();
        }

        UpdateMode();
        Move();
        ReleaseGhost();

    }

    //Nimmt den Gegner der näher dran ist als seinen Gegner den er angreift
    void UpdatePlayer() {
       
        if (players.Length == 2) {
            float dis1 = Vector3.Distance(players[0].transform.position, transform.position);
            float dis2 = Vector3.Distance(players[1].transform.position, transform.position);

            if (dis1 > dis2) {
                player = players[0];
            } else if (dis1 < dis2) {
                player = players[1];
            } else {
                int r = Random.Range(0, 1);

                player = players[r];
            }
        }

        if (players.Length == 0) {
            Debug.Log("Leer");
        }
    }

    void ReleaseGhost() {
        releaseTimer += Time.deltaTime;

        if (releaseTimer > releaseTimeGhost) {
            isReleased = true;
        }
    }

    //Ecken wo sie im Scatter Mode hinlaufen
    Waypoint GetScatterWayPoint() {
        if (ghostType == GhostType.Blinky) {
            foreach (Waypoint wp in GameObject.Find("Waypoints").GetComponent<WaypointsControl>().waypointsList) {
                if (wp.name.Equals("Waypoint Scatter Blinky")) {
                    return wp;
                }
            }
        } else if (ghostType == GhostType.Pinky) {
            foreach (Waypoint wp in GameObject.Find("Waypoints").GetComponent<WaypointsControl>().waypointsList) {
                if (wp.name.Equals("Waypoint Scatter Pinky")) {
                    return wp;
                }
            }
        } else if (ghostType == GhostType.Inky) {
            foreach (Waypoint wp in GameObject.Find("Waypoints").GetComponent<WaypointsControl>().waypointsList) {
                if (wp.name.Equals("Waypoint Scatter Inky")) {
                    return wp;
                }
            }
        } else if (ghostType == GhostType.Clyde) {
            foreach (Waypoint wp in GameObject.Find("Waypoints").GetComponent<WaypointsControl>().waypointsList) {
                if (wp.name.Equals("Waypoint Scatter Clyde")) {
                    return wp;
                }
            }
        }

        return null;
    }

    //Ändert den Modus
    void ChangeMode(Mode mode) {
        if (mode == Mode.Frightened && currentMode == Mode.Frightened) {
            return;
        }

        previousMode = currentMode;
        currentMode = mode;

        if (currentMode == Mode.Frightened) {
            speed = frightenedSpeed;
        } else {
            speed = normalSpeed;
        }
    }

    public void StartFrightenedMode() {
        if (currentMode != Mode.Consumed) {
            frightenedModeTimer = 0.0f;

            ChangeMode(Mode.Frightened);
        }

    }

    //Den Modus nach verstrichener Zeit ändern
    //2 Scatter Zeiten, Die erste länger als die zweite; und 2 Chase Zeit, der 2. Chase dauert bis zum Ende, kein erneuter Wechsel 
    //Scatter -> Chase -> Scatter -> Chase
    void UpdateMode() {
        if (currentWayPoint != spawnWayPoint) {
            isInHouse = false;
        }

        if (currentMode != Mode.Frightened) {
            GetComponent<MeshFilter>().mesh = initialMesh;
            GetComponent<Renderer>().material = initialMaterial;
            transform.GetChild(0).gameObject.GetComponent<Renderer>().enabled = true;
            transform.GetChild(1).gameObject.GetComponent<Renderer>().enabled = false;
            transform.GetChild(2).gameObject.GetComponent<Renderer>().enabled = false;

            sprite = transform.GetChild(0);
            sprite.rotation = spriteRotation;

            speed = normalSpeed;

            if (changeModeCount == 1) {
                changeModeTimer += Time.deltaTime;

                if (currentMode == Mode.Scatter && changeModeTimer > scatterTime1) {
                    ChangeMode(Mode.Chase);
                    changeModeTimer = 0;
                } else if (currentMode == Mode.Chase && changeModeTimer > chaseTime) {
                    ChangeMode(Mode.Scatter);

                    changeModeCount = 2;
                    changeModeTimer = 0;
                }
            } else if (changeModeCount == 2) {
                changeModeTimer += Time.deltaTime;

                if (currentMode == Mode.Scatter && changeModeTimer > scatterTime1) {
                    ChangeMode(Mode.Chase);
                    changeModeTimer = 0;
                } else if (currentMode == Mode.Chase && changeModeTimer > chaseTime) {
                    ChangeMode(Mode.Scatter);

                    changeModeCount = 3;
                    changeModeTimer = 0;
                }
            } else if (changeModeCount == 3) {
                changeModeTimer += Time.deltaTime;

                if (currentMode == Mode.Scatter && changeModeTimer > scatterTime2) {
                    ChangeMode(Mode.Chase);
                    changeModeTimer = 0;
                } else if (currentMode == Mode.Chase && changeModeTimer > chaseTime) {
                    ChangeMode(Mode.Scatter);

                    changeModeCount = 4;
                    changeModeTimer = 0;
                }

            } else if (changeModeCount == 4) {
                changeModeTimer += Time.deltaTime;

                if (currentMode == Mode.Scatter && changeModeTimer > scatterTime2) {
                    ChangeMode(Mode.Chase);
                    changeModeTimer = 0;
                }
            }

        } else if (currentMode == Mode.Frightened) {
            GetComponent<MeshFilter>().mesh = frightenedMesh;
            GetComponent<Renderer>().material = frightenedMaterial;
            transform.GetChild(0).gameObject.GetComponent<Renderer>().enabled = false;
            transform.GetChild(1).gameObject.GetComponent<Renderer>().enabled = true;

            sprite = transform.GetChild(1);
            spriteRotation = sprite.rotation;

            frightenedModeTimer += Time.deltaTime;

            if (frightenedModeTimer > frightenedTime) {
                ChangeMode(previousMode);
                frightenedModeTimer = 0.0f;
            }

            if (frightenedModeTimer >= blinkingTime) {
                blinkTimer += Time.deltaTime;
                if (blinkTimer >= .1f) {
                    if (GetComponent<MeshFilter>().mesh != blinkMesh) {
                        GetComponent<MeshFilter>().mesh = blinkMesh;
                        GetComponent<Renderer>().material = blinkMaterial;
                        transform.GetChild(1).gameObject.GetComponent<Renderer>().enabled = false;
                        transform.GetChild(2).gameObject.GetComponent<Renderer>().enabled = true;

                        sprite = transform.GetChild(2);
                        sprite.rotation = spriteRotation;
                    } else {
                        GetComponent<MeshFilter>().mesh = frightenedMesh;
                        GetComponent<Renderer>().material = frightenedMaterial;
                        transform.GetChild(1).gameObject.GetComponent<Renderer>().enabled = true;
                        transform.GetChild(2).gameObject.GetComponent<Renderer>().enabled = false;

                        sprite = transform.GetChild(1);
                        sprite.rotation = spriteRotation;
                    }

                    blinkTimer = 0.0f;

                }


            }
        }
    }

    //Bewegung zum WayPoint
    void Move() {
        if (targetWayPoint != currentWayPoint && targetWayPoint != null) {
            if (OverShotTargetWayPoint()) {
                currentWayPoint = targetWayPoint;
                transform.position = currentWayPoint.transform.position;

                Waypoint receivingWayPoint = GetPortal(currentWayPoint.transform.position);
                if (receivingWayPoint != null) {
                    transform.position = receivingWayPoint.transform.position;
                    currentWayPoint = receivingWayPoint;
                }

                targetWayPoint = ChooseNextWayPoint();
                previousWayPoint = currentWayPoint;
                currentWayPoint = null;
                ChangeOrientation();
            } else {
                transform.position += currentDirection * speed * Time.deltaTime;
            }
        }
    }

    /**
     * PacMan's aktuelle Position wird genommen
     * targetPos = playerPos
     * PacMan Dossier: using Pac-Man's current tile as his target
     */
    Vector3 BlinkyTarget() {
        Vector3 playerPos = player.transform.position;
        Vector3 targetPos = playerPos;

        return targetPos;
    }

    /**
     * Berechnet 4 Felder vor dem PacMan
     * targetPos = playerPos + 4*Richtung 
     * bsp: playerPos = (12, 4, 0) Richtung = links --> targetPos = playerPos + 4 * (-1, 0, 0) = (8, 4, 0)
     * 
     * 
     *  PacMan Dossier: Pinky behaves as he does because he does not target Pac-Man's tile directly. Instead, he selects an offset four tiles away from Pac-Man in the direction Pac-Man is currently moving 
     */
    Vector3 PinkyTarget() {
        Vector3 playerPos = player.transform.position;
        Vector3 targetPos = Vector3.zero;
        if (!multiplayer) {
            targetPos = playerPos + (4 * player.GetComponent<PlayerControl>().GetDirection());
        } else {
            targetPos = playerPos + (4 * player.GetComponent<MultiplayerControl>().GetDirection());
        }
        return targetPos;
    }

    /**
     * Nimmt Blinky's Position und addiert darauf den Offset zwischen Blinky und berechneten Position 4 Felder vor PacMan
     * targetPos = blinkyPos + Distanz(Blinky, Player + 4*Richtung)
     * bsp: playerPos = (12, 4, 0) Richtung = links blinkyPos = (23, 4, 3) newPlayerPos + 4*Richtung = (12, 4, 0) + 3 * (-4, 0, 0) = (8, 4, 0)
     * (random Wert genommen nicht richtiger Wert!) Distanz = (blinkyPos, newPlayerPos) = 3
     * targetPos = (23 + 3, 4, 3 + 3) = (26, 4, 6)
     * 
     * PacMan Dossier: Inky uses the most complex targeting scheme of the four ghosts in chase mode. He needs Pac-Man's current tile/orientation and Blinky's current tile to calculate his final target
     */
    Vector3 InkyTarget() {
        Vector3 playerPos = player.transform.position;
        Vector3 targetPos = Vector3.zero;
        if (!multiplayer) {
            targetPos = playerPos + (4 * player.GetComponent<PlayerControl>().GetDirection());
        } else {
            targetPos = playerPos + (4 * player.GetComponent<MultiplayerControl>().GetDirection());
        }

        Vector3 blinkyPos = GameObject.Find("Blinky").transform.position;

        float distance = Vector3.Distance(blinkyPos, targetPos);
        targetPos = new Vector3(blinkyPos.x + distance, blinkyPos.y, blinkyPos.z + distance);

        return targetPos;
    }

    /**
     * Wie Blinky nur bei einer Distance zwischen PacMan und Clyde von 8 ändert sich sein Target zu seinem Scatter Waypoint
     * Wenn Distanz(Clyde, Player) > 8 --> targetPos = PlayerPos (wie Blinky) sonst targetPos = Scatter
     * 
     * PacMan Dossier: Clyde's target differs based on his proximity to Pac-Man. When more than eight tiles away, he uses Pac-Man's tile as his target (shown as the yellow target above). If Clyde is closer than eight tiles away, he switches to his scatter mode target
     */
    Vector3 ClydeTarget() {
        Vector3 playerPos = player.transform.position;
        float distance = Vector3.Distance(transform.position, playerPos);
        Vector3 targetPos = Vector3.zero;

        if (distance > 8.0f) {
            targetPos = playerPos;
        } else {
            targetPos = scatterWayPoint.transform.position;
        }

        return targetPos;
    }

    /**
     * Nimmt die Position von oben ^
     * und sucht den kürzesten Abstand zu einem der benachbarten WayPoints zu dem der Ghost dann läuft
     *
     */
    Waypoint ChooseNextWayPoint() {
        Vector3 targetPos = Vector3.zero;
        if (currentMode == Mode.Chase) {
            if (ghostType == GhostType.Blinky) {
                targetPos = BlinkyTarget();
            } else if (ghostType == GhostType.Pinky) {
                targetPos = PinkyTarget();
            } else if (ghostType == GhostType.Inky) {
                targetPos = InkyTarget();
            } else if (ghostType == GhostType.Clyde) {
                targetPos = ClydeTarget();
            }
        } else {
            targetPos = scatterWayPoint.transform.position;
        }

        Waypoint moveToWayPoint = null;
        Waypoint[] possibleWayPoints = new Waypoint[4];
        Vector3[] possibleDirections = new Vector3[4];

        int counter = 0;

        //Sucht mögliche erreichbare WayPoints, umdrehen ist für die Ghosts nicht möglich
        for (int i = 0; i < currentWayPoint.neighbours.Length; i++) {
            if (currentWayPoint.validDirections[i] != currentDirection * -1) {
                possibleWayPoints[counter] = currentWayPoint.neighbours[i];
                possibleDirections[counter] = currentWayPoint.validDirections[i];
                counter++;
            }
        }

        if (possibleWayPoints.Length == 1) {
            moveToWayPoint = possibleWayPoints[0];
            currentDirection = possibleDirections[0];
        } else if (possibleWayPoints.Length > 1) {
            float minDistance = 100000.0f;                                                                  //Mit minDistance wird verglichen zwischen den einzelnen WayPoints zu welchem ein Ghost den Kürzesten Weg hat

            for (int i = 0; i < possibleWayPoints.Length; i++) {
                if (possibleDirections[i] != Vector3.zero) {
                    if (Vector3.Distance(possibleWayPoints[i].transform.position, targetPos) < minDistance) {
                        minDistance = Vector3.Distance(possibleWayPoints[i].transform.position, targetPos);
                        moveToWayPoint = possibleWayPoints[i];
                        currentDirection = possibleDirections[i];
                    }
                }
            }
        }

        return moveToWayPoint;
    }

    Waypoint GetWayPointAtPos(Vector3 pos) {
        List<Waypoint> waypoints = GameObject.Find("Waypoints").GetComponent<WaypointsControl>().waypointsList;
        foreach (Waypoint waypoint in waypoints) {
            if (pos == waypoint.transform.position) {
                return waypoint;
            }
        }

        return null;
    }

    Waypoint GetPortal(Vector3 pos) {
        List<Waypoint> waypoints = GameObject.Find("Waypoints").GetComponent<WaypointsControl>().waypointsList;
        foreach (Waypoint waypoint in waypoints) {
            if (pos == waypoint.transform.position) {
                if (waypoint.GetComponent<Waypoint>().isPortal) {
                    Waypoint receiver = waypoint.GetComponent<Waypoint>().receiver.GetComponent<Waypoint>();
                    return receiver;
                }
            }
        }

        return null;
    }

    bool OverShotTargetWayPoint() {
        float distancePrevToTargetWayPoint = Vector3.Distance(targetWayPoint.transform.position, previousWayPoint.transform.position);
        float distancePrevToSelf = Vector3.Distance(transform.position, previousWayPoint.transform.position);

        return distancePrevToSelf > distancePrevToTargetWayPoint;
    }

    //Drehen der Ghosts
    void ChangeOrientation() {
        if (currentDirection == Vector3.forward) {
            transform.localRotation = Quaternion.Euler(0.0f, 90.0f, 0.0f);
        } else if (currentDirection == Vector3.back) {
            transform.localRotation = Quaternion.Euler(0.0f, -90.0f, 0.0f);
        } else if (currentDirection == Vector3.right) {
            transform.localRotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
        } else if (currentDirection == Vector3.left) {
            transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
        }

        sprite.rotation = spriteRotation;
    }

    //Wenn ein Ghost gefressen wurde, wechselt er in den Consumed Mode und wird zurück zum Spawn teleportiert und wartet bevor er wieder startet
    private void Consumed() {
        ChangeMode(Mode.Consumed);

        transform.position = spawnWayPoint.transform.position;
        currentWayPoint = spawnWayPoint;

        targetWayPoint = null;
        currentDirection = Vector3.left;
        StartCoroutine(WaitAfterConsumed(2.5f));
    }

    //Collision mit dem Player
    private void OnTriggerEnter(Collider other) {
        if (other.tag.Equals("Player") && currentMode == Mode.Frightened) {
            Consumed();

            if (!multiplayer) {
                PillsControl.points += 200;
            } else {
                other.GetComponent<MultiplayerControl>().points += 200;
            }
        } else if (other.tag.Equals("Player") && currentMode != Mode.Frightened) {
            if (!multiplayer) {
                other.GetComponent<PlayerControl>().CheckCollision();

            } else {
                other.GetComponent<MultiplayerControl>().CheckCollision();

            }
        }
    }

    private IEnumerator WaitToRelease(float waitTime) {
        yield return new WaitForSeconds(waitTime);

        if (isInHouse) {
            targetWayPoint = currentWayPoint.neighbours[0];
            currentDirection = currentWayPoint.validDirections[0];
        }
    }

    private IEnumerator WaitAfterConsumed(float waitTime) {
        yield return new WaitForSeconds(waitTime);

        currentMode = Mode.Chase;

        targetWayPoint = ChooseNextWayPoint();
        previousWayPoint = currentWayPoint;
        currentWayPoint = null;
        ChangeOrientation();
    }

}